#  Hacemos la funcion de suma
def sumar(a, b):
    return a + b

# Hacemos la funcion de resta
def restar(a, b):
    return a - b

# Suma de 1 + 2
print("La suma es igual a: " + str(sumar(1, 2)))

# Suma de 3 + 4
print("La suma es igual a: " + str(sumar(3, 4)))

# Resta de 6 - 5
print("La resta es igual a: " + str(restar(6, 5)))

# Resta de 8 - 7
p("La resta es igual a: " + str(restar(8, 7)))